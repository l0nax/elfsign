//
// Created by Francesco Emanuel Bennici <benniciemanuel78@gmail.com> on 12.12.2018.
//

#include "main.h"
#include "../lib/elf/elf.h"

/// ---------- Variables -----------
elfsign_opts *opts;

/// ---------- Code -----------
int main(int argc, char *argv[]) {
    int numArgs;

    if(argc == 1) {
        printHelp(argv[0]);
        exit(0);
    }

    // init used Keyfile
    opts->keyfile = NULL;

    // get Commandline Args
    numArgs = process_options(argc, argv);
    if(numArgs <= 0) {
        printHelp(argv[0]);
        exit(0);
    }

    if(opts->mode == SIGN) {

    } else if(opts->mode == VERIFY) {

    } else if(opts->mode == DUMP) {

    }

    return 0;
}
