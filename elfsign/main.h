//
// Created by Francesco Emanuel Bennici <benniciemanuel78@gmail.com> on 12.12.2018.
//

#ifndef ELFSIGN_MAIN_H
#define ELFSIGN_MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include <getopt.h>

#define ELFSIGN_VERSION "1.0.0"

/// ---------- Arguments ----------
static struct option long_opts[7] = {
    {"sign", 0, 0, 's'},
    {"help", 0, 0, 'h'},
    {"version", 0, 0, 'v'},
    {"dump", 0, 0, 'd'},
    {"verify", 0, 0, 'V'},
    {"keyfile", 0, 0, 'k'},
    {0, 0, 0, 0}
};

typedef struct _elfsign_opts {
    char *keyfile; /// RSA Keyfile witch will be used to Sign the Binary

    /// operation Modes
    enum {
        NONE,
        SIGN,
        VERIFY,
        DUMP
    } mode;
} elfsign_opts;

/// ---------- Variables ----------
extern elfsign_opts *opts;

/// ---------- Functions ----------
static inline void printVersion(void) { printf("elfsign - v%s\n", ELFSIGN_VERSION); }

/**
 * Print the Help Message
 *
 * @param name  The Name of the Application (eg argv[0])
 */
static void printHelp(char *name) {
    printVersion();
    printf("\nSyntax: %s [COMMAND] [options] file\n", name);
    printf("\nCommands:\n"
           " -s, --sign                     Sign File\n"
           " -h, --help                     Print this Message and exit\n"
           " -v, --version                  Print Version and exit\n"
           " -d, --dump                     Print Informations about Executable File (signature, ...)\n"
           " -V, --verify                   Verify Binary File\n"
           " -k, --keyfile                  Set the RSA Key File witch should be used to Sign the Binary\n"
    );
}

/**
 * Process the Options given by User
 *
 * @param argc  Number of Arguments
 * @param argv  Char Pointer Array of Arguments
 * @return      Returns the Number of passed Arguments or -1 if there was an Error
 */
static int process_options(int argc, char *const *argv) {
    int ret, len;

    len = strlen(argv[0]);
    if (len >= 7 && strncmp(argv[0] + len - 7, "elfsign", 8) == 0)
        opts->mode = SIGN;
    else if (len >= 8 && strncmp(argv[0] + len - 8, "elfverify", 10) == 0)
        opts->mode = VERIFY;

    ret = 1;
    for (;;) {
        int c, option_index;
        int _break = 0;

        c = getopt_long(argc, argv, "svVhk:",
                        long_opts, &option_index);

        switch (c) {
            case 's': opts->mode = SIGN;
                break;

            case 'V': opts->mode = VERIFY;
                break;

            case 'd': opts->mode = DUMP;
                break;

            case 'v': printVersion();
                exit(0);

            case 'h': printHelp(argv[0]);
                exit(0);

            case 'k': opts->keyfile = optarg;
                break;

            default:
                _break = 1;
                break;
        }

        if(_break)
            break;

        /* count number of processed options */
        ret = optind;
    }

    if (opts->mode == NONE)
        return -1;

    return ret;
}

#endif //ELFSIGN_MAIN_H
