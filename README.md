# elfsign

This Project has the following Goals:
1. Creating the possibility to sign and verify ELF Files
2. Creating a Kernel Module to protect Linux Systems before executing
   non-signed Executables/ Libraries

## Used Libraries

- [libelf](https://packages.debian.org/de/sid/libelf1) (included in Package _[elfutils](https://packages.debian.org/de/sid/elfutils)_)

## Getting Started

_Coming Soon_